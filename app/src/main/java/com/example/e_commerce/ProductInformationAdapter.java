package com.example.e_commerce;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ProductInformationAdapter extends RecyclerView.Adapter<ProductInformationAdapter.ViewHolder> {

    private List<ProductInformationModel> productInformationModelList;

    public ProductInformationAdapter(List<ProductInformationModel> productInformationModelList) {
        this.productInformationModelList = productInformationModelList;
    }



    @NonNull
    @Override
    public ProductInformationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_information_item_layout,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductInformationAdapter.ViewHolder viewHolder, int position) {
        String featureTitle = productInformationModelList.get(position).getFeatureName();
        String featureDetail = productInformationModelList.get(position).getFeatureValue();
        viewHolder.setFeatures(featureTitle,featureDetail);
    }

    @Override
    public int getItemCount() {
        return productInformationModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView featureName;
        private TextView featureValue;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            featureName = itemView.findViewById(R.id.feature_name);
            featureValue = itemView.findViewById(R.id.feature_value);
        }
        private void setFeatures(String featureTitle, String featureDetail){
            featureName.setText(featureTitle);
            featureValue.setText(featureDetail);
        }


    }
}
