package com.example.e_commerce.ui.account;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.example.e_commerce.DBqueries;
import com.example.e_commerce.MyAddressActivity;
import com.example.e_commerce.R;

public class MyAccountFragment extends Fragment {
    private Dialog loadingDialog;
    public MyAccountFragment() {
    }

    private Button viewAllAddressBtn;
    public static final int MANAGE_ADDRESS = 1;             //để quản lý chọn 1 addres ở phần chọn address

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//////////loading dialog
        loadingDialog = new Dialog(getContext());
        loadingDialog.setContentView(R.layout.loading_progress_dialog);
        loadingDialog.setCancelable(false);
        loadingDialog.getWindow().setBackgroundDrawable(getContext().getDrawable(R.drawable.slider_background));
        loadingDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        loadingDialog.show();
//////////loading dialog

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);
        viewAllAddressBtn = view.findViewById(R.id.view_all_addreses_btn);
        viewAllAddressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addressIntent = new Intent(getContext(), MyAddressActivity.class);
                addressIntent.putExtra("MODE", MANAGE_ADDRESS);
                startActivity(addressIntent);
            }
        });
        DBqueries.loadOrders(getContext(),null,loadingDialog);
        return view;
    }
}