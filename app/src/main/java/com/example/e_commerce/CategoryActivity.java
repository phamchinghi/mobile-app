package com.example.e_commerce;

import static com.example.e_commerce.DBqueries.lists;
import static com.example.e_commerce.DBqueries.loadedCategoriesName;
import static com.example.e_commerce.DBqueries.setCategoryData;
import static com.example.e_commerce.DBqueries.setFragmentData;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.e_commerce.ui.home.HomePageAdapter;
import com.example.lib.Model.HomePageModel;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends AppCompatActivity {

    private RecyclerView categoryRecyclerView;
    private HomePageAdapter adapter;
    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = getIntent().getStringExtra("CategoryName");
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        switch(title){
            case "Áo":
                title = "AO";
                break;
            case "Quần":
                title = "QUAN";
                break;
            case "Áo khoác":
                title = "AO_KHOAC";
                break;
            case "Kính":
                title = "KINH";
                break;
            case "Nón":
                title = "NON";
                break;
            default:
                break;
        }

        categoryRecyclerView = findViewById(R.id.category_recyclerview);
        LinearLayoutManager testingLayoutManager = new LinearLayoutManager(this);
        testingLayoutManager.setOrientation(LinearLayout.VERTICAL);
        categoryRecyclerView.setLayoutManager(testingLayoutManager);

        int positionOfList = 0;
        for(int i = 0;i<loadedCategoriesName.size();i++){
            if(loadedCategoriesName.get(i).equals(title.toUpperCase())){
                positionOfList = i;
            }
        }
        if(positionOfList == 0){
            loadedCategoriesName.add(title.toUpperCase());
            lists.add(new ArrayList<HomePageModel>());
            adapter = new HomePageAdapter(lists.get(loadedCategoriesName.size()-1));
            setCategoryData(this,adapter,loadedCategoriesName.size()-1,title);
        }else{
            adapter = new HomePageAdapter(lists.get(positionOfList));
        }

        categoryRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; thêm các item vào action bar nếu như nó hiển thị
        getMenuInflater().inflate(R.menu.search_icon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.main_search_icon){
            //To do search here
            startActivity(new Intent(this,SearchActivity.class));
            return true;
        }else if(id == android.R.id.home){      //
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}