package com.example.lib.Model;

public class CategoryModel {
    //Tạo trước 2 thuộc tính để design, các thuộc tính còn lại dựa vào thiết kế
    private String CategoryLink;
    private String CategoryName;

    public CategoryModel(String categoryLink, String categoryName) {
        CategoryLink = categoryLink;
        CategoryName = categoryName;
    }

    public String getCategoryLink() {
        return CategoryLink;
    }

    public void setCategoryLink(String categoryLink) {
        CategoryLink = categoryLink;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }
}
