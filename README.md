## Mobile-App

**An E-commerce Android app**

#### This app have:
1. Splash Screen
2. Sign-in
3. Sign-up
4. Sign-out
5. Forgot password
6. Cart
7. Wishlist
8. Custom account
9. Order and order detail
10. Search
11. Notification
12. Firebase
---

Home page | Splash Screen | Menu
--------- | ------------- | -------
<img src="./app/images/home.jpg" width="300" height="600"> | <img src="./app/images/splash.jpg" width="300" height="600"> | <img src="./app/images/menu.jpg" width="300" height="600">

Sign-in | Forgot Password | Search
------- | --------------- | ---------
<img src="./app/images/signin.jpg" width="300" height="600"> | <img src="./app/images/forgotpass.jpg" width="300" height="600"> | <img src="./app/images/timkiem.jpg" width="300" height="600">
